const mongoose = require('mongoose')

const PopunderSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    initialDelay: {
      type: Number,
      required: true,
    },
    secondDelay: {
      type: Number,
      required: true,
    },
    cycleDelay: {
      type: Number,
      required: true,
    },
    mobileEnabled: {
      type: Boolean,
      required: true,
      default: true,
    },
    tabletEnabled: {
      type: Boolean,
      required: true,
      default: true,
    },
    webEnabled: {
      type: Boolean,
      required: true,
      default: true,
    },
    linkOne: {
      type: String,
      required: true,
    },
    linkTwo: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    perPageCookies: {
      type: Boolean,
      required: false,
      default: false
    }
  }
)

PopunderSchema.virtual('publisher', {
  ref: 'Publisher',
  localField: 'ownerId',
  foreignField: '_id',
  justOne: true,
})

module.exports = mongoose.model('Popunder', PopunderSchema)
