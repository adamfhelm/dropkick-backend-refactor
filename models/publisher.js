const mongoose = require('mongoose')

const PublisherSchema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    createdAt: {
      type: Date,
    },
  }
)

PublisherSchema.virtual('popunders', {
  ref: 'Popunder',
  localField: '_id',
  foreignField: 'ownerId',
  justOne: false,
})

module.exports = mongoose.model('Publisher', PublisherSchema)
