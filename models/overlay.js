const mongoose = require('mongoose')

const OverlaySchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    iframeUrl: {
      type: String,
      required: true,
    },
    width: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
    initialDelay: {
      type: Number,
      required: true,
    },
    intervalDelay: {
      type: Number,
      required: true,
    }
  }
)

OverlaySchema.virtual('publisher', {
  ref: 'Publisher',
  localField: 'ownerId',
  foreignField: '_id',
  justOne: true,
})

module.exports = mongoose.model('Overlay', OverlaySchema)
