const mongoose = require('mongoose')

const FloaterSchema = new mongoose.Schema(
  {
    ownerId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    createdAt: {
      type: Date,
      required: true,
    },
    updatedAt: {
      type: Date,
      required: true,
    },
    vastUrl: {
      type: String,
      required: false,
    },
    iframeUrl: {
      type: String,
      required: false,
    },
    mobileIframeUrl: {
      type: String,
      required: false,
    },
    startDelay: {
      type: Number,
      required: true,
      default: 2000,
    },
    totalTime: {
      type: Number,
      rqeuired: true,
      default: 2000,
    },
    resetTime: {
      type: Number,
      required: true,
      default: 86400
    },
    webEnabled: {
      type: Boolean,
      required: true,
      default: true,
    },
    mobileEnabled: {
      type: Boolean,
      required: true,
      default: true,
    }
  }
)

FloaterSchema.virtual('publisher', {
  ref: 'Publisher',
  localField: 'ownerId',
  foreignField: '_id',
  justOne: true,
})

module.exports = mongoose.model('Floater', FloaterSchema)
