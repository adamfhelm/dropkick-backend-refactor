const auth = require('../middleware/auth')
const path = require('path')
const fs = require('fs')
const mongoose = require('mongoose')
const Overlay = mongoose.model('Overlay')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/overlays', auth, error(createOverlay))
  app.get('/overlays', auth, error(loadOverlays))
  app.get('/overlays/:configId', error(loadScript))
  app.put('/overlays/:configId', auth, error(updateOverlay))
  app.delete('/overlays/:configId', auth, error(deleteOverlay))
}

async function createOverlay(req, res) {
  const { _doc } = await Overlay.create({
    ...req.body,
    createdAt: new Date(),
    updatedAt: new Date(),
  })
  res.json(_doc)
}

async function updateOverlay(req, res) {
  const { configId } = req.params
  if (!configId) {
    res.status(400).json({ message: 'No configId supplied in request' })
    return
  }
  const { n } = await Overlay.updateOne({
    _id: mongoose.Types.ObjectId(configId),
  }, {
    ...req.body,
    _id: undefined,
    updatedAt: new Date(),
  }, {
    omitUndefined: true,
    runValidators: true,
  })
  if (n === 1) {
    res.status(204).end()
  } else {
    res.status(400).json({ message: 'Did not modify one document' })
  }
}

async function deleteOverlay(req, res) {
  const { configId } = req.params
  if (!configId) {
    res.status(400).json({ message: 'No configId supplied' })
    return
  }
  const result = await Overlay.findOneAndDelete({
    _id: mongoose.Types.ObjectId(configId)
  }).exec()
  if (result) {
    res.status(204).end()
  } else {
    res.status(404).end()
  }
}

async function loadOverlays(req, res) {
  const { publisherId } = req.query
  if (!publisherId) {
    res.status(400).json({ message: 'Supply a publisherId to load configs' })
    return
  }
  const docs = await Overlay.find({
    ownerId: mongoose.Types.ObjectId(publisherId),
  }).populate('publisher').lean().exec()
  res.json(docs)
}

async function loadScript(req, res) {
  try {
    const { configId } = req.params
    if (!configId) {
      res.status(400).json({ message: 'No configId found in query params' })
      return
    }
    const config = await Overlay.findOne({
      _id: mongoose.Types.ObjectId(configId),
    }).lean().exec()
    if (!config) {
      res.status(404).json({ message: `Unable to find config "${configId}"` })
      return
    }
    const filepath = process.env.NODE_ENV === 'production' ? '../dist/overlay.min.js' : '../scripts/overlay.js'
    let overlay = await loadFile(path.resolve(__dirname, filepath))
    overlay = overlay.replace('<<IFRAME_URL>>', config.iframeUrl)
    overlay = overlay.replace('<<WIDTH>>', config.width)
    overlay = overlay.replace('<<HEIGHT>>', config.height)
    overlay = overlay.replace('<<INITIAL_DELAY>>', config.initialDelay)
    overlay = overlay.replace('<<INTERVAL_DELAY>>', config.intervalDelay)
    res.set('Cache-Control', `max-age=0, s-maxage=${60 * 30}, stale-while-revalidate`)
    res.setHeader('Content-Type', 'application/javascript')
    res.send(overlay)
  } catch (err) {
    console.log('Error loading overlay script', err)
    res.status(500).end()
  }
}

async function loadFile(filepath) {
  return await new Promise((rs, rj) => {
    fs.readFile(filepath, (err, data) => {
      if (err) return rj(err)
      rs(data.toString('utf8'))
    })
  })
}
