const auth = require('../middleware/auth')
const path = require('path')
const fs = require('fs')
const mongoose = require('mongoose')
const Popunder = mongoose.model('Popunder')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/popunders', auth, error(createPopunder))
  app.get('/popunders', auth, error(loadPopunders))
  app.get('/popunders/:configId', error(loadScript))
  app.put('/popunders/:configId', auth, error(updatePopunder))
  app.delete('/popunders/:configId', auth, error(deletePopunder))
}

async function createPopunder(req, res) {
  const { _doc } = await Popunder.create({
    ...req.body,
    createdAt: new Date(),
    updatedAt: new Date(),
  })
  res.json(_doc)
}

async function updatePopunder(req, res) {
  const { configId } = req.params
  if (!configId) {
    res.status(400).json({ message: 'No configId supplied in url'})
    return
  }
  const { n } = await Popunder.updateOne({
    _id: mongoose.Types.ObjectId(configId)
  }, {
    ...req.body,
    _id: undefined,
    updatedAt: new Date(),
  }, {
    omitUndefined: true,
    runValidators: true,
  })
  if (n === 1) {
    res.status(204).end()
  } else {
    res.status(400).json({ message: 'Did not modify one document' })
  }
}

async function deletePopunder(req, res) {
  const { configId } = req.params
  if (!configId) {
    res.status(400).json({ message: 'No configId supplied' })
    return
  }
  const result = await Popunder.findOneAndDelete({
    _id: mongoose.Types.ObjectId(configId)
  }).exec()
  if (result) {
    res.status(204).end()
  } else {
    res.status(404).end()
  }
}

async function loadPopunders(req, res) {
  const { publisherId } = req.query
  if (!publisherId) {
    res.status(400).json({ message: 'Supply a publisherId to load configs' })
    return
  }
  const docs = await Popunder.find({
    ownerId: mongoose.Types.ObjectId(publisherId),
  }).populate('publisher').lean().exec()
  res.json(docs)
}

async function loadScript(req, res) {
  try {
    const { configId } = req.params
    if (!configId) {
      res.status(400).json({ message: 'No configId found in query params' })
      return
    }
    const config = await Popunder.findOne({
      _id: mongoose.Types.ObjectId(configId),
    }).lean().exec()
    if (!config) {
      res.status(404).json({ message: `Unable to find config "${configId}"` })
      return
    }
    const filepath = process.env.NODE_ENV === 'production' ? '../dist/popunder.min.js' : '../scripts/popunder.js'
    let popunder = await loadFile(path.resolve(__dirname, filepath))
    res.set('Cache-Control', `max-age=0, s-maxage=${60 * 30}, stale-while-revalidate`)
    popunder = popunder.replace('<<POPUNDER_LINK>>', config.linkOne)
    popunder = popunder.replace('<<POPUNDER_LINK_TWO>>', config.linkTwo)
    popunder = popunder.replace('<<FIRST_DELAY>>', +config.initialDelay * 1000)
    popunder = popunder.replace('<<SECOND_DELAY>>', +config.secondDelay * 1000)
    popunder = popunder.replace('<<MOBILE_ENABLED>>', config.mobileEnabled.toString())
    popunder = popunder.replace('<<TABLET_ENABLED>>', config.tabletEnabled.toString())
    popunder = popunder.replace('<<WEB_ENABLED>>', config.webEnabled.toString())
    popunder = popunder.replace('<<CYCLE_DELAY>>', config.cycleDelay * 1000)
    popunder = popunder.replace('<<PER_PAGE_COOKIES>>', (!!config.perPageCookies).toString())
    res.setHeader('Content-Type', 'application/javascript')
    res.send(popunder)
  } catch (err) {
    console.log('Error loading popunder script', err)
    res.status(500).end()
  }
}

async function loadFile(filepath) {
  return await new Promise((rs, rj) => {
    fs.readFile(filepath, (err, data) => {
      if (err) return rj(err)
      rs(data.toString('utf8'))
    })
  })
}
