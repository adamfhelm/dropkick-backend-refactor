const auth = require('../middleware/auth')
const path = require('path')
const fs = require('fs')
const mongoose = require('mongoose')
const Floater = mongoose.model('Floater')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/floaters', auth, error(createFloater))
  app.get('/floaters', auth, error(loadFloaters))
  app.get('/floaters/:configId', error(loadScript))
  app.put('/floaters/:configId', auth, error(updateFloater))
  app.delete('/floaters/:configId', auth, error(deleteFloater))
}

async function createFloater(req, res) {
  const { _doc } = await Floater.create({
    ...req.body,
    createdAt: new Date(),
    updatedAt: new Date(),
  })
  res.json(_doc)
}

async function updateFloater(req, res) {
  const { configId } = req.params
  if (!configId) {
    res.status(400).json({ message: 'No configId supplied in request' })
    return
  }
  const { n } = await Floater.updateOne({
    _id: mongoose.Types.ObjectId(configId),
  }, {
    ...req.body,
    _id: undefined,
    updatedAt: new Date(),
  }, {
    omitUndefined: true,
    runValidators: true,
  })
  if (n === 1) {
    res.status(204).end()
  } else {
    res.status(400).json({ message: 'Did not modify one document' })
  }
}

async function deleteFloater(req, res) {
  const { configId } = req.params
  if (!configId) {
    res.status(400).json({ message: 'No configId supplied' })
    return
  }
  const result = await Floater.findOneAndDelete({
    _id: mongoose.Types.ObjectId(configId)
  }).exec()
  if (result) {
    res.status(204).end()
  } else {
    res.status(404).end()
  }
}

async function loadFloaters(req, res) {
  const { publisherId } = req.query
  if (!publisherId) {
    res.status(400).json({ message: 'Supply a publisherId to load configs' })
    return
  }
  const docs = await Floater.find({
    ownerId: mongoose.Types.ObjectId(publisherId),
  }).populate('publisher').lean().exec()
  res.json(docs)
}

async function loadScript(req, res) {
  try {
    const { configId } = req.params
    if (!configId) {
      res.status(400).json({ message: 'No configId found in query params' })
      return
    }
    const config = await Floater.findOne({
      _id: mongoose.Types.ObjectId(configId),
    }).lean().exec()
    if (!config) {
      res.status(404).json({ message: `Unable to find config "${configId}"` })
      return
    }
    const filepath = process.env.NODE_ENV === 'production' ? '../dist/floater.min.js' : '../scripts/floater.js'
    let floater = await loadFile(path.resolve(__dirname, filepath))
    floater = floater.replace('<<IFRAME_URL>>', config.iframeUrl)
    floater = floater.replace('<<MOBILE_IFRAME_URL>>', config.mobileIframeUrl || '')
    floater = floater.replace('<<START_TIME>>', config.startDelay)
    floater = floater.replace('<<TOTAL_TIME>>', config.totalTime)
    floater = floater.replace('<<RESET_TIME>>', config.resetTime)
    floater = floater.replace('<<MOBILE_ENABLED>>', config.mobileEnabled.toString())
    floater = floater.replace('<<WEB_ENABLED>>', config.webEnabled.toString())
    floater = floater.replace('<<VAST_URL>>', config.vastUrl || '')
    res.set('Cache-Control', `max-age=0, s-maxage=${60 * 30}, stale-while-revalidate`)
    res.setHeader('Content-Type', 'application/javascript')
    res.send(floater)
  } catch (err) {
    console.log('Error loading floater script', err)
    res.status(500).end()
  }
}

async function loadFile(filepath) {
  return await new Promise((rs, rj) => {
    fs.readFile(filepath, (err, data) => {
      if (err) return rj(err)
      rs(data.toString('utf8'))
    })
  })
}
