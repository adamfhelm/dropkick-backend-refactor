const auth = require('../middleware/auth')
const mongoose = require('mongoose')
const Popunder = mongoose.model('Popunder')
const Publisher = mongoose.model('Publisher')
const error = require('../middleware/catch_error')

module.exports = (app) => {
  app.post('/publishers', auth, error(createPublisher))
  app.get('/publishers', auth, error(loadPublishers))
}

async function loadPublishers(req, res) {
  const publishers = await Publisher.find().lean().exec()
  res.json(publishers)
}

async function createPublisher(req, res) {
  const existing = await Publisher.findOne({
    name: req.body.name,
  }).exec()
  if (existing) {
    res.status(422).json({ message: 'A publisher with this name already exists' })
    return
  }
  const { _doc } = await Publisher.create({
    ...req.body,
    createdAt: new Date(),
  })
  res.json(_doc)
}
