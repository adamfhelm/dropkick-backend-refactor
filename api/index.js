const fs = require("fs");
// const mongoose = require("mongoose");
// mongoose.set("useCreateIndex", true);
// require("../models/popunder");
// require("../models/publisher");
//require("./models/user");
// require("../models/floater");
// require("../models/overlay");
const express = require("express");
const app = express();

app.get("/test", async (req, res) => {
  try {
    if (!req.query.embed) {
      res.status(400);
      res.json({ message: 'Send "embed" as a URI encoded query parameter' });
      return;
    }
    const html = await loadFile(path.resolve(__dirname, "index.html"));
    res.set("X-XSS-Protection", "0");
    res.send(html.replace(/<<EMBED>>/g, decodeURI(req.query.embed)));
  } catch (err) {
    console.log("Error generating test html file", err);
    res.status(500).end();
  }
});

async function loadFile(filepath) {
  return await new Promise((rs, rj) => {
    fs.readFile(filepath, (err, data) => {
      if (err) return rj(err);
      rs(data.toString("utf8"));
    });
  });
}
