const path = require('path')

module.exports = {
  mode: 'development',
  node: {
    fs: 'empty',
    net: 'empty',
    tls: 'empty'
  },
  entry: {
    popunder: './scripts/popunder.js',
    floater: './scripts/floater.js',
    overlay: './scripts/overlay.js',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            // presets: ['@babel/preset-env']
          }
        }
      }
    ]
  },
  output: {
    filename: '[name].min.js',
  }
}
