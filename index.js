const path = require("path");
const fs = require("fs");
const mongoose = require("mongoose");
mongoose.set("useCreateIndex", true);
// require("../models/popunder");
// require("../models/publisher");
require("./models/user");
// require("../models/floater");
// require("../models/overlay");
const express = require("express");
const app = express();
// const cors = require("cors");

app.use(express.json());
// app.use(cors());
app.use((req, res, next) => {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
  res.set(
    "Access-Control-Allow-Headers",
    "Origin, Content-Type, access-control-allow-origin"
  );
  next();
});

// Redirect for old domain
// app.use((req, res, next) => {
//   if (req.hostname === 'scripts.dropkick-media.now.sh') {
//     res.writeHead(301, {
//       'Cache-Control': 'max-age=2592000, s-maxage=2592000',
//       'Location': `https://scripts.dkmedia.now.sh${req.url}`
//     })
//     res.end()
//     return
//   }
//   next()
// })

// Connect to mongo on request
app.use(async (req, res, next) => {
  await mongoose.connect(process.env.DB_URI, {
    connectTimeoutMS: 5000,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  next();
});

// expose user route
// RESTful routes.

require("./api/users")(app);

//require("./test")(app);
// require("./publishers")(app);
// require("./popunder")(app);
// require("./floater")(app);
// require("./overlay")(app);

module.exports = app;
