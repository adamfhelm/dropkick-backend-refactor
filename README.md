# popunder

The repo for managing the popup script logic.

## Build System

This project uses webpack to minify the Javascript code in the `dist` folder. Example use can be seen in `index.html`.
