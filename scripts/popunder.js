const isMobile = require('ismobilejs').default;

// Time values are converted to ms in route handler
const popup_first_delay = +'<<FIRST_DELAY>>'; //first display after x milliseconds
const popup_second_delay = +'<<SECOND_DELAY>>'; //second display after x milliseconds 20
const popup_mobile_traffic = '<<MOBILE_ENABLED>>' === 'true'; // Do we target mobile ? true/false
const popup_tablets_traffic = '<<TABLET_ENABLED>>' === 'true'; // Do we target tablets ? true/false
const popup_web_traffic = '<<WEB_ENABLED>>' === 'true'; // Do we target desktop  ? true/false
const per_page_cookies = '<<PER_PAGE_COOKIES>>' === 'true'

// TODO: accept these as query parameters in the server serving the JS script
const popunder_link = '<<POPUNDER_LINK>>';
const popunder_link_two = '<<POPUNDER_LINK_TWO>>';
const popup_delay = +'<<CYCLE_DELAY>>';
const stay_on_my_website = true;

const follow_link = true

const COOKIE_STATUS_ID = 'dkmckStatus';
const COOKIE_FIRST_LOAD = 'dkmckFirstAccess';
const COOKIE_FIRST_CLICK = 'dkmckFirstClick';
const COOKIE_SECOND_CLICK = 'dkmckSecondClick';

const domain = location.hostname.split('.').reverse()[1];
const tld = location.hostname.split('.').reverse()[0];
SetCookie('domain', `${domain}.${tld}`);
SetCookie('path', location.pathname);

if (!GetCookie(COOKIE_FIRST_LOAD)){
  // console.log('first access')
  const now = new Date().getTime();
  SetCookie(COOKIE_FIRST_LOAD, now);
}

if (!GetCookie(COOKIE_STATUS_ID)) {
  // console.log('No status')
  SetCookie(COOKIE_STATUS_ID, 0);
}

function cycleResetTime() {
  const dkmckFirstClick = GetCookie(COOKIE_FIRST_CLICK);
  const dkmckSecondClick = GetCookie(COOKIE_SECOND_CLICK);
  return +(dkmckSecondClick || Infinity) + popup_delay;
}

function firstPopupTime() {
  const dkmckFirstAccess = GetCookie(COOKIE_FIRST_LOAD);
  return +dkmckFirstAccess + popup_first_delay;
}

function secondPopupTime() {
  const dkmckFirstClick = GetCookie(COOKIE_FIRST_CLICK);
  return +(dkmckFirstClick || Infinity) + popup_second_delay;
}

document.addEventListener('DOMContentLoaded', PopupInitPu);

// Functions are hoisted
// https://developer.mozilla.org/en-US/docs/Glossary/Hoisting#Learn_more

function PopupInitPu() {
  if (document.attachEvent) {
    const links = document.querySelectorAll('a')
    if (links.addEventListener) {
      links.addEventListener('onclick', PopupCheckTargetLink, true)
    } else if (links.attachEvent) {
      links.attachEvent('onclick', PopupCheckTargetLink);
    } else {
      console.log('Unable to attach event listener to link elements')
    }
  } else if (document.addEventListener) {
    const links = document.querySelectorAll('a');
    for (let link of links) {
      link.addEventListener('click', PopupCheckTargetLink, false)
    }
  }
}

//GO TO THE LINK ON NEW TAB, ADVERTISER LINK BEHIND
function PopupCheckTargetLink(e) {
  const dkmckStatus = GetCookie(COOKIE_STATUS_ID);
  const _now = new Date().getTime();

  if (!isDeviceActive()) {
    console.log('This user agent is not active for popunder')
    return true
  }
  // console.log(_now, firstPopupTime(), cycleResetTime())
  // console.log(dkmckStatus)
  // console.log('status: ', dkmckStatus, _now, firstPopupTime())
  if (
      (dkmckStatus === '0' && _now > firstPopupTime()) ||
      (dkmckStatus === '1' && _now > secondPopupTime())
    ) {
    e.stopPropagation();
    // Find the target href by cycling up the element tree
    if (!follow_link) {
      window.open(window.location.href, '_blank', '', '');
    } else if (e.srcElement.href) {
      window.open(e.srcElement.href, '_blank', '', '');
    } else {
      let parent = e.srcElement.parentElement
      while (parent) {
        if (parent.href) {
          window.open(parent.href, '_blank', '', '');
          break
        }
        parent = parent.parentElement
      }
      if (!parent) {
        // Unable to source an href from the DOM tree, just use the current href
        window.open(window.location.href, '_blank', '', '');
      }
    }

    if (dkmckStatus === '0') {
      SetCookie(COOKIE_STATUS_ID, 1);
      SetCookie(COOKIE_FIRST_CLICK, _now);
      window.open(popunder_link, '_self', '', '');
    } else if (dkmckStatus === '1') {
      SetCookie(COOKIE_STATUS_ID, 2);
      SetCookie(COOKIE_SECOND_CLICK, _now);
      window.open(popunder_link_two, '_self', '', '');
    }
    e.preventDefault();
    return false;
  } else if (dkmckStatus === '2' && _now > cycleResetTime()) {
    SetCookie(COOKIE_STATUS_ID, 0);
    SetCookie(COOKIE_FIRST_LOAD, _now);
    DeleteCookie(COOKIE_FIRST_CLICK);
    DeleteCookie(COOKIE_SECOND_CLICK);
    return PopupCheckTargetLink(e);
  }
}

function isDeviceActive() {
  const ua = navigator.userAgent.toLowerCase();
  const tablet = isMobile(ua).tablet;
  const mobile = isMobile(ua).phone;
  const web = !tablet && !mobile;

  return (popup_mobile_traffic && mobile) ||
    (popup_tablets_traffic && tablet) ||
    (popup_web_traffic && web);
}

function SetCookie(name, value) {
  let expires = ' '
  if (name === COOKIE_STATUS_ID) {
    const _expires = new Date();
    _expires.setTime(_expires.getTime() + popup_delay);
    expires = ` expires=${_expires.toGMTString()}; `;
  }
  document.cookie = `${name}=${value};${expires}${per_page_cookies ? '' : 'path=/;'}`;
}

function GetCookie(name) {
  const cookies = document.cookie.toString().split(';');
  for (let cookie of cookies) {
    const [c_name, c_value] = cookie.split('=')
    if (c_name.trim() == name) {
      return c_value;
    }
  }
  return null;
}

function DeleteCookie(name) {
  document.cookie = `${name}=; path=/;`;
}

function reset() {
  document.cookie = `expires=${new Date().toGMTString()}; path=/;`;
}
