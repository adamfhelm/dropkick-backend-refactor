const isMobile = require('ismobilejs').default
const { VASTClient, VASTParser } = require('vast-client')
const { default: videojs } = require('video.js')
const get = require('lodash.get')

let dkm_start = dkm_timer = null
const dkm_height = 250
const dkm_startTime = +'<<START_TIME>>'
const dkm_totalTime = +'<<TOTAL_TIME>>'
const iframe_url = '<<IFRAME_URL>>'
const mobileIframeUrl = '<<MOBILE_IFRAME_URL>>'
const dkm_resetTime = +'<<RESET_TIME>>'
const dkm_mobileEnabled = '<<MOBILE_ENABLED>>' === 'true'
const dkm_webEnabled = '<<WEB_ENABLED>>' === 'true'
const dkm_vastUrl = '<<VAST_URL>>'

const COOKIE_STATUS_ID = 'dkm_floater_status'

if (dkm_vastUrl) {
  const stylesheet = document.createElement('link')
  stylesheet.setAttribute('rel', 'stylesheet')
  stylesheet.setAttribute('href', 'https://vjs.zencdn.net/7.6.6/video-js.css')
  document.head.appendChild(stylesheet)
}

/**
 * Element building functions
 **/
const floaterElement = () => {
  const element = document.createElement('div')
  element.setAttribute('style', `
    width: 300px;
    height: 250px;
    position: fixed;
    right: 0px;
    z-index: 999;
    border: 0 none;
    background: transparent;
    visibility: hidden;
  `)
  return element
}

const iframeElement = () => {
  const element = document.createElement('iframe')
  element.setAttribute('src', iframe_url)
  element.setAttribute('width', '300')
  element.setAttribute('height', dkm_height || '250')
  element.setAttribute('frameborder', '0')
  element.setAttribute('scrolling', 'no')
  element.setAttribute('style', `
    position: relative;
    right: 0px;
    bottom: 0px;
  `)
  return element
}

const closeButtonElement = () => {
  const element = document.createElement('a')
  element.innerHTML = 'X'
  element.setAttribute('href', 'javascript:void(0)')
  element.setAttribute('onclick', 'window.dkm_closeFloatingAd()')
  element.setAttribute('style', `
    display: block;
    position: absolute;
    z-index: 1001;
    top: 0px;
    right: 0px;
    width: 16px;
    height: 16px;
    background: #000;
    line-height: 16px;
    text-align: center;
    overflow: hidden;
    color: #fff;
    font-size: 16px;
    font-family: arial;
    cursor: hand;
    cursor: pointer;
  `)
  return element
}

const relativeContainerElement = () => {
  const element = document.createElement('div')
  element.setAttribute('width', '300')
  element.setAttribute('style', `
    position: relative;
  `)
  return element
}

const videoElement = () => {
  const element = document.createElement('video')
  element.setAttribute('id', 'vast_player')
  // element.setAttribute('width', '300')
  // element.setAttribute('height', '250')
  element.setAttribute('class', 'video-js vjs-default-skin')
  element.setAttribute('playsinline', 'true')
  element.setAttribute('style', `
    position: relative !important;
    width: 100% !important;
    height: 100% !important;
    object-position: 50% 50%;
    object-fit: fit;
  `)
  return element
}

const mobileFloaterElement = () => {
  const element = document.createElement('div')
  element.setAttribute('style', `
    height: 100px;
    position: fixed;
    left: 0px;
    right: 0px;
    bottom: 0px;
    border: 0 none;
    background: transparent;
    z-index: 999;
    display: flex;
    align-items: center;
    justify-content: center;
  `)
  return element
}

const mobileContainerElement = () => {
  const element = document.createElement('div')
  element.setAttribute('style', `
    position: absolute;
    width: 300px;
  `)
  return element
}

const mobileIframeElement = () => {
  const element = document.createElement('iframe')
  element.setAttribute('src', mobileIframeUrl || iframe_url)
  element.setAttribute('width', '300')
  element.setAttribute('height', '100')
  element.setAttribute('frameborder', '0')
  element.setAttribute('scrolling', 'no')
  return element
}

const mobileCloseButtonElement = () => {
  const element = document.createElement('a')
  element.setAttribute('href', 'javascript:void(0)')
  element.setAttribute('onclick', 'window.dkm_closeFloatingAd()')
  element.setAttribute('style', `
    display: block;
    position: absolute;
    z-index: 1001;
    top: 0px;
    right: 0px;
    width: 16px;
    height: 16px;
    background: #000;
    line-height: 16px;
    text-align:center;
    overflow: hidden;
    color: #fff;
    font-size: 16px;
    font-family: arial;
  `)
  element.innerHTML = 'X'
  return element
}


const dkm_floater = floaterElement()
const dkm_mobileFloater = mobileFloaterElement()

async function initializePlayer(elementId) {
  const vastClient = new VASTClient()
  const { ads } = await vastClient.get(dkm_vastUrl)
  const {
    mediaFiles,
    trackingEvents,
    videoClickThroughURLTemplate,
    videoClickTrackingURLTemplates
  } = get(ads, '[0].creatives[0]', {})
  if (!mediaFiles || !trackingEvents) {
    console.log(
      'Failed to load mediaFiles and trackingEvents',
      mediaFiles,
      trackingEvents
    )
    return
  }
  // console.log(Object.keys(trackingEvents))
  pingUrls(trackingEvents.creativeView)
  const element = document.getElementById(elementId)
  const cover = document.createElement('div')
  cover.setAttribute('style', `
    position: absolute;
    left: 0px;
    right: 0px;
    bottom: 25px;
    top: 16px;
    background-color: transparent;
    z-index: 1200;
  `)
  cover.addEventListener('click', () => {
    pingUrls(videoClickTrackingURLTemplates)
    window.open(videoClickThroughURLTemplate, '_blank')
  })
  element.parentElement.appendChild(cover)
  const instance = videojs(element, {})
  let stopped = true
  instance.on('play', () => {
    if (stopped) {
      stopped = false
      pingUrls(trackingEvents.start)
      return
    }
    pingUrls(trackingEvents.resume)
  })
  instance.on('pause', () => {
    pingUrls(trackingEvents.pause)
  })
  instance.on('volumechange', (e) => {
    const volume = instance.volume()
    const muted = instance.muted()
    if (+volume === 0 || muted) {
      pingUrls(trackingEvents.mute)
    }
    if (+volume > 0 && !muted) {
      pingUrls(trackingEvents.unmute)
    }
  })
  let lastOffset = 2
  instance.on('timeupdate', () => {
    const duration = instance.duration()
    const offset = instance.currentTime()
    const position = offset / duration
    const firstQuartile = duration / 4 / duration
    const secondQuartile = duration / 2 / duration
    const thirdQuartile = 3 * duration / 4 / duration
    if (lastOffset !== 2 && position > thirdQuartile) {
      lastOffset = 2
      pingUrls(trackingEvents.thirdQuartile)
    } else if (lastOffset !== 1 && position < thirdQuartile && position > secondQuartile) {
      lastOffset = 1
      pingUrls(trackingEvents.midpoint)
    } else if (lastOffset !== 0 && position < secondQuartile && position > firstQuartile) {
      lastOffset = 0
      pingUrls(trackingEvents.firstQuartile)
    }
  })
  instance.on('fullscreenchange', (e) => {
    pingUrls(trackingEvents.fullscreen)
  })
  const playableFiles = mediaFiles.filter((media) => {
    const canPlay = instance.canPlayType(media.mimeType)
    return canPlay === 'probably' || canPlay === 'maybe'
  })
  let adIndex = 0
  instance.on('ended', () => {
    pingUrls(trackingEvents.complete)
    adIndex = (adIndex + 1) % playableFiles.length
    instance.src(playableFiles[adIndex].fileURL)
    instance.play()
  })
  instance.src(playableFiles[adIndex].fileURL)
  instance.muted(true)
  instance.loop()
  instance.controls(true)
  instance.autoplay(true)
}

window.animateIn = function(element, height = dkm_height, animationTime = 2000) {
  const startTime = new Date().getTime()
  const animate = () => {
    const progress = Math.min(animationTime, new Date().getTime() - startTime)
    element.style.bottom = height * (1 - Math.pow((progress / animationTime - 1), 4)) - height + 'px'
    if (progress < dkm_totalTime) {
      // Animate at 30 fps
      window.setTimeout(animate, 32)
    } else {
      element.style.position = 'fixed'
      element.style.bottom = '0px'
    }
  }
  animate()
}

window.dkm_closeFloatingAd = () => {
  dkm_floater.remove()
  dkm_mobileFloater.remove()
}

const dkm_onLoad = async () => {
  if (GetCookie(COOKIE_STATUS_ID) !== null) {
    // console.log('Skipping floater display')
    return
  }
  setTimeout(async () => {
    const tablet = isMobile().tablet
    const mobile = isMobile().phone
    if (dkm_mobileEnabled && (tablet || mobile)) {
      // Mobile
      const mobileContainer = mobileContainerElement()
      dkm_mobileFloater.appendChild(mobileContainer)
      if (dkm_vastUrl) {
        const container = relativeContainerElement()
        container.appendChild(videoElement())
        mobileContainer.appendChild(videoElement())
      } else if (iframe_url) {
        mobileContainer.appendChild(mobileIframeElement())
      } else {
        throw new Error('Floater should have either vast url or iframe url')
      }
      mobileContainer.appendChild(mobileCloseButtonElement())
      document.body.appendChild(dkm_mobileFloater)
      // dkm_timer = window.animateIn(dkm_mobileFloater, dkm_totalTime)
    } else if (dkm_webEnabled && !tablet && !mobile) {
      // Web
      if (dkm_vastUrl) {
        const container = relativeContainerElement()
        container.appendChild(videoElement())
        dkm_floater.appendChild(container)
      } else if (iframe_url) {
        const container = relativeContainerElement()
        container.appendChild(iframeElement())
        dkm_floater.appendChild(container)
      } else {
        throw new Error('Floater should have either vast url or iframe url')
      }

      dkm_floater.appendChild(closeButtonElement())
      document.body.appendChild(dkm_floater)
      dkm_floater.style.visibility = 'visible';
      dkm_timer = window.animateIn(dkm_floater, dkm_totalTime)
    } else {
      return
    }
    if (dkm_vastUrl) {
      await initializePlayer('vast_player')
    }
    SetCookie(COOKIE_STATUS_ID, 1)
  }, dkm_startTime)
}

if (window.addEventListener) window.addEventListener('load', dkm_onLoad, false);
else if (window.attachEvent) window.attachEvent('onload', dkm_onLoad);

function SetCookie(name, value) {
  let expires = ' '
  if (name === COOKIE_STATUS_ID) {
    const _expires = new Date();
    _expires.setTime(_expires.getTime() + dkm_resetTime * 1000);
    expires = ` expires=${_expires.toGMTString()}; `;
  }
  document.cookie = `${name}=${value};${expires}path=/;`;
}

function GetCookie(name) {
  const cookies = document.cookie.toString().split(';');
  for (let cookie of cookies) {
    const [c_name, c_value] = cookie.split('=')
    if (c_name.trim() == name) {
      return c_value;
    }
  }
  return null;
}

function DeleteCookie(name) {
  document.cookie = `${name}=; path=/;`;
}

// Sends a GET request to an array of urls
function pingUrls(urls = []) {
  return Promise.all(urls.map(async (_url) => await fetch(_url)))
}
