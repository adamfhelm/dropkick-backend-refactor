
const iframeUrl = '<<IFRAME_URL>>'
const width = +'<<WIDTH>>'
const height = +'<<HEIGHT>>'
const initialDelay = +'<<INITIAL_DELAY>>'
const intervalDelay = +'<<INTERVAL_DELAY>>'

const overlayId = 'dkm_overlay_popup'

const COOKIE_STATUS_ID = 'dkm_overlay_status'
const COOKIE_NEXT_SHOW = 'dkm_next_overlay_show'

if (window.addEventListener) window.addEventListener('load', onLoad, false)
else if (window.attachEvent) window.attachEvent('onload', onLoad)

const iframeOverlay = () => {
  const container = document.createElement('div')
  container.setAttribute('style', `
    display: flex;
    flex-direction: column;
    align-items: flex-end;
  `)
  const element = document.createElement('iframe')
  element.setAttribute('src', iframeUrl)
  element.setAttribute('width', `${width}px`)
  element.setAttribute('height', `${height}px`)
  element.setAttribute('frameborder', '0')
  element.setAttribute('scrolling', 'no')
  const overlay = overlayElement()
  overlay.setAttribute('id', overlayId)
  container.appendChild(closeButton())
  container.appendChild(element)
  overlay.appendChild(container)
  return overlay
}

const closeButton = () => {
  const element = document.createElement('div')
  element.innerHTML = 'X'
  element.addEventListener('click', () => closeOverlay)
  element.setAttribute('style', `
    cursor: pointer;
    color: white;
    font-weight: bold;
    font-size: 20px;
    font-family: Helvetica;
  `)
  return element
}

const overlayElement = () => {
  const element = document.createElement('div')
  element.addEventListener('click', () => closeOverlay())
  element.setAttribute('style', `
    position: fixed;
    top: 0px;
    left: 0px;
    right: 0px;
    bottom: 0px;
    background-color: rgba(0, 0, 0, 0.8);
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  `)
  return element
}

function closeOverlay() {
  // Close the popup if it exists in the DOM
  const overlay = document.getElementById(overlayId)
  if (!overlay) return
  overlay.remove()
  SetCookie(COOKIE_NEXT_SHOW, +new Date() + intervalDelay)
  showIfNeeded()
}

function onLoad() {
  const nextShowDate = GetCookie(COOKIE_NEXT_SHOW)
  if (+nextShowDate && +new Date() >= nextShowDate) {
    setTimeout(() => {
      showIfNeeded()
    }, initialDelay)
  } else {
    showIfNeeded()
  }
}

function showIfNeeded() {
  const nextShowDate = GetCookie(COOKIE_NEXT_SHOW)
  if (!nextShowDate) {
    // Treat as initial load
    setTimeout(() => {
      document.body.appendChild(iframeOverlay())
    }, initialDelay)
  } else if (+new Date() >= nextShowDate) {
    DeleteCookie(COOKIE_NEXT_SHOW)
    document.body.appendChild(iframeOverlay())
  } else if (+new Date() < nextShowDate) {
    setTimeout(() => {
      showIfNeeded()
    }, nextShowDate - +new Date())
  }
}

function SetCookie(name, value) {
  let expires = ' '
  if (name === COOKIE_STATUS_ID) {
    const _expires = new Date();
    _expires.setTime(_expires.getTime() + dkm_resetTime);
    expires = ` expires=${_expires.toGMTString()}; `;
  }
  document.cookie = `${name}=${value};${expires}path=/;`;
}

function GetCookie(name) {
  const cookies = document.cookie.toString().split(';');
  for (let cookie of cookies) {
    const [c_name, c_value] = cookie.split('=')
    if (c_name.trim() == name) {
      return c_value;
    }
  }
  return null;
}

function DeleteCookie(name) {
  document.cookie = `${name}=; path=/;`;
}
