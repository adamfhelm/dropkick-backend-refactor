FROM node:10
MAINTAINER Chance Hudson

COPY . /src
WORKDIR /src

RUN npm ci

ENV NODE_ENV production
CMD ["node", "."]
